# Weather App

## Authors

-   Alexander Gotfrid
-   Arthur Saraiva

## Description

-   Weather app built with Python

## App File Structure

-   weather-app/src
    -   utility
        -   html_parser.py
    -   scrape_weather.py
    -   db_operations.py
    -   weather_processor.py
    -   plot_operations.py
    -   test_final_project.py
