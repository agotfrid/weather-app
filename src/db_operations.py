# PART 2: Module with a DBOperations class
"""
This Module Utilizes DBOperations class, in order to create data table
"""
import sqlite3
from utility.utility import Utility


class DBOperations:
    """
    The DBOperations class allows to open sqlite db connection
    and create/update the table using a dictionary
    """

    def __init__(self, dbname=None):
        """ Initialize database name and initializes the database"""
        if dbname is None:
            self.dbname = "weather.sqlite"
        else:
            self.dbname = dbname
        self.initialize_db()

    def initialize_db(self):
        """ Initializes the database and creates a samples table if it does not exist"""
        try:
            with DBCM(self.dbname) as cursor:
                cursor.execute(
                    """create table if not exists samples
                                (id integer primary key autoincrement not null,
                                sample_date text not null,
                                location text not null,
                                min_temp real not null,
                                max_temp real not null,
                                avg_temp real not null);"""
                )
                cursor.execute(
                    """
                    CREATE UNIQUE INDEX if not exists idx_sample_dates
                    ON samples (sample_date);
                    """
                )
        except Exception as e_table:
            print("Error initializing table:", e_table)

    def save_data(self, weather_dict):
        """ Inserts weather data into table by taking data as dictionary of dictionaries"""
        for sample_date, val in weather_dict.items():
            try:
                sql = """
                    replace into samples (sample_date,location,min_temp,max_temp,avg_temp)
                                            values (?,?,?,?,?)
                    """
                data = (
                    sample_date,
                    "Winnipeg, MB",
                    val["Min"],
                    val["Max"],
                    val["Mean"],
                )
                with DBCM(self.dbname) as cursor:
                    cursor.execute(sql, data)
                print(f"Saving to the database.")
            except Exception as e_insert:
                print("Error inserting weather data.", e_insert)

    def fetch_data(self, start_year, end_year, latest=False, month=None):
        """ Will return the requested data for plotting depending on the parameters passed"""
        collection = {
            "1": [],
            "2": [],
            "3": [],
            "4": [],
            "5": [],
            "6": [],
            "7": [],
            "8": [],
            "9": [],
            "10": [],
            "11": [],
            "12": [],
        }
        try:
            with DBCM(self.dbname) as cursor:
                if latest:  # runs query for latest data
                    return_data = cursor.execute(
                        "select sample_date, avg_temp from samples "
                        "order by sample_date desc limit 1"
                    )
                    for row in return_data:
                        return row[0]
                elif month is not None:  # runs query based on passed month
                    return_data = cursor.execute(
                        "select sample_date, avg_temp from samples WHERE CAST(SUBSTR("
                        "sample_date,0,5) AS int) BETWEEN ? AND ? AND CAST(SUBSTR("
                        "sample_date,6,8) AS int) IS ?"
                        "AND avg_temp NOT LIKE "
                        '"Missing" order by sample_date asc',
                        (str(start_year), str(end_year), str(month)),
                    )
                    for row in return_data:
                        collection["%d" % int(row[0][5:7])].append(
                            {"day": row[0][8:11], "temp": row[1]}
                        )
                else:  # runs query for all dates
                    return_data = cursor.execute(
                        "select sample_date, avg_temp from samples WHERE CAST(SUBSTR("
                        "sample_date,0,5) AS int) BETWEEN ? AND ? AND avg_temp NOT LIKE "
                        '"Missing"',
                        (str(start_year), str(end_year)),
                    )
                    for row in return_data:
                        collection["%d" % int(row[0][5:7])].append(row[1])
            return collection
        except Exception as e_print:
            print("Error fetching table information.", e_print)

    def purge_data(self):
        """ Will purge all the data from the DB for when the program fetches all new weather data"""
        try:
            sql_select = """DELETE from samples"""
            with DBCM(self.dbname) as cursor:
                cursor.execute(sql_select)
        except Exception as e_print:
            print("Error purging table.", e_print)


class DBCM:
    """ Context manager for database connections"""

    def __init__(self, dbname):
        """ Initializes context manager variables"""
        try:
            self.dbname = dbname
        except Exception as e_print:
            print("Error initializing context.", e_print)

    def __enter__(self):
        """ Executes the setup code """
        try:
            self.conn = sqlite3.connect(self.dbname)
            self.cursor = self.conn.cursor()
            return self.cursor
        except Exception as db_error:
            print("Error opening DB:", db_error)

    def __exit__(self, exc_type, exc_value, exc_traceback):
        try:
            self.conn.commit()
            self.cursor.close()
            self.conn.close()
        except Exception as db_error:
            print("Error closing DB:", db_error)


if __name__ == "__main__":
    databasename = "weather.sqlite"
    db_operation = DBOperations(databasename)
    # db_operation.purge_data()
    # db_operation.initialize_db()
    # result = db_operation.fetch_data(2020, 2020, False)
    # result = db_operation.fetch_data(2020, 2020, False, 11)
    result = db_operation.fetch_data(2020, 2020, True)
    print(result)
