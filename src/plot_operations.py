# PART 3: Module with a PlotOperations class

"""
This Module Utilizes matplotlib to plot a plot
"""
import matplotlib.pyplot as plt
import numpy as np
from db_operations import DBOperations


class PlotOperations:
    """
    The PlotOperations class allows to plot two weather graphs
    """

    def __init__(self, weather_data=None):
        """ Initialize class variables"""
        super().__init__()
        if weather_data is None:
            self.weather_data = {}
        else:
            self.weather_data = weather_data

    def plot_yearly_data(self, start_y, end_y):
        """ Plots data for each month of the year in the year range """
        weather_data = DBOperations().fetch_data(start_y, end_y, False)

        labels, data = weather_data.keys(), weather_data.values()

        plt.boxplot(data)
        plt.xticks(range(1, len(labels) + 1), labels)
        plt.title(f"Monthly Temperature Distribution for: {start_y} to {end_y}")
        plt.ylabel("Temperature (Celsius)")
        plt.xlabel("Days")
        plt.show()

    def plot_month_data(self, year, month):
        """ Plots data for specified month and year"""
        data = DBOperations().fetch_data(year, year, False, month)
        temperature = [record["temp"] for record in data[str(month)]]
        days = [record["day"] for record in data[str(month)]]

        plt.plot(days, temperature, label="Temperature")
        plt.ylabel("Temperature (Celsius)")
        plt.xlabel("Days")
        plt.show()


if __name__ == "__main__":
    # PlotOperations("weather.sqlite").plot_yearly_data(1997, 2020)
    PlotOperations("weather.sqlite").plot_month_data(2020, 11)
