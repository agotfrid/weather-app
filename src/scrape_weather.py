"""
This Module Utilizes ParseHTML class parse weather data
from climate.weather.gc.ca.
"""
from utility.html_parser import ParseHTML
from utility.utility import Utility
from db_operations import DBOperations
from html.parser import HTMLParser
import urllib.request


class WeatherScraper:
    """
    The WeatherScraper class............
    """

    def __init__(self):
        """ Initialize class variables"""
        try:
            super().__init__()
            self.parser = ""
            self.weather = {}
            self.location = ""
        except Exception as e:
            print("Error: ", e)

    def start_scraping(self, url, year, stop_at_date=None):
        """This method will call scrape_data,
        and as long as there is a previous month to be checked,
        it will keep calling scrape_data for each month, until it
        finds no more new data.
        :stop_at_date: is optional, should be passed
                       as a string in yyyy-mm format (2020-08)
        """
        try:
            request_counter = 0
            current_year = year
            current_month = Utility.get_month(url)
            if (
                stop_at_date is not None
                and current_year == stop_at_date[:4]
                and current_month == stop_at_date[5:7]
            ):
                return
            self.scrape_data(url)
            self.location = Utility.get_location(url)
            request_counter += 1
            print(
                f"Scrapped data for: {request_counter} ==> Year {current_year} Month {current_month}"
            )
            while self.parser.is_more_data_available:
                try:
                    current_month = Utility.get_previous_month(current_month)
                    if current_month == "12":
                        current_year = Utility.get_previous_year(current_year)
                    if (
                        stop_at_date is not None
                        and current_year == stop_at_date[:4]
                        and current_month == stop_at_date[5:7]
                    ):
                        break
                    url = Utility.get_url(current_year, current_month)
                    self.scrape_data(url)
                    # Check stop_at_date params. When passed, will only check until this month
                    # stop_at_date in "yyyy-mm" format
                    request_counter += 1
                    print(
                        f"Scrapped data for: {request_counter} ==> Year {current_year} Month {current_month}"
                    )
                    # Uncomment the following code for testing, will make it parse data for just 2 months
                    # self.parser.is_more_data_available = False
                except Exception as e:
                    print("Error: ", e)
        except Exception as e:
            print("Error: ", e)

    def scrape_data(self, url):
        """This method will call the url using ParseHTML parser class,
        to scrape weather data from climate.weather.gc.ca
        """
        try:
            retries = 0
            while retries < 2:
                try:
                    # To be used with threading
                    # First get year and month from url param
                    year = Utility.get_year(url)
                    month = Utility.get_month(url)
                    # instantiate ParseHTML class
                    self.parser = ParseHTML(year, month)
                    # parse data contained in the url
                    with urllib.request.urlopen(url) as response:
                        html = str(response.read())
                    self.parser.feed(html)
                    # Update self.weather dictionary with parsed data
                    self.weather.update(self.parser.weather_data)
                    return
                except Exception as e:
                    retries += 1
        except Exception as e:
            print("Error: ", e)


if __name__ == "__main__":
    weather_scraper = WeatherScraper()
    url = (
        "https://climate.weather.gc.ca/climate_data/daily_data_e.html?StationID=27174&timeframe=2&StartYear=1840"
        "&EndYear=2020&Day=30&Year=2020&Month=12# "
    )
    # Scrappe all data
    # weather_scraper.start_scraping(url, "2020")
    # Scrappe newest data until 2020-08
    weather_scraper.start_scraping(url, "2020", "2020-08")

    weather_dict = weather_scraper.weather
    dbname = "weather.sqlite"
    db_operation = DBOperations(dbname)
    # db_operation.purge_data()
    db_operation.save_data(weather_dict)
    print(
        "Scrapped data from ",
        len(weather_scraper.weather),
        " days",
    )
