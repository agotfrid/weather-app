"""
This Module Utilizes XHTML Parsing using HTMLParser and urrlib.parser to parse
weather data from climate.weather.gc.ca
"""
from html.parser import HTMLParser
import urllib.request

# This is needed just for testing, will be transfered to scrape_weather later
from datetime import datetime


class ParseHTML(HTMLParser):
    """
    The ParseHTML class allows to extract weather data from climate.weather.gc.ca
    """

    def __init__(self, year, month):
        """Initialize class variables and helper flags"""
        try:
            super().__init__()
            self.weather_data = {}
            ### Helper Flags for parsing ###
            self.current_tag = ""
            self.tbody = False
            self.entered_row = False
            self.row_header = False
            self.day_data = False
            self.get_data = False
            self.data_counter = 0
            self.invalid_row = False
            # This flag is used to tell that no more older data is available for grabbing
            self.is_more_data_available = False
            ### Dictionary Data ###
            self.dict_year = year
            self.dict_month = month
            self.dict_day = 0
            self.dict_max_temp = 0
            self.dict_min_temp = 0
            self.dict_mean_temp = 0
        except Exception as e:
            print("Error: ", e)

    def handle_starttag(self, tag, attrs):
        """Handles start tags to setup flags"""
        try:
            # Entered tbody tag
            if "tbody" in tag:
                self.tbody = True
            if self.tbody:
                # Entered <tr> tag
                if "tr" in tag:
                    self.entered_row = True
                if self.entered_row:
                    # Entered <th> tag
                    if "th" in tag:
                        # When on <th> check for scope attribute, which means is a row header
                        # that usually contains the day
                        for attr in attrs:
                            if attr[0] == "scope" and attr[1] == "row":
                                self.row_header = True
                    # Entered <td> tag
                    if "td" in tag:
                        # self.data_counter flag is used to count the first three
                        # columns needed to get the weather data
                        if self.data_counter == 0:
                            # self.get_data flag is used to know, inside handle_data,
                            # if data should be used to generate the dictionary
                            self.get_data = True
                if self.row_header:
                    # Entered <a> tag, which means it is a row header
                    # with a day (there are a few rows that contains other kind of data)
                    if "abbr" in tag:  # got inside the row header
                        self.day_data = True
                        self.data_counter = 0
            if "li" == tag:
                for attr in attrs:
                    if (
                        attr[0] == "class"
                        and "previous" in attr[1]
                        and "disabled" not in attr[1]
                    ):
                        # This flag is used to tell that no more older data is available for grabbing
                        self.is_more_data_available = True
        except Exception as e:
            print("Error: ", e)

    def handle_data(self, data):
        """Handles Data inside the tags, based on the flags is able to grab the necessary data"""
        try:
            if self.row_header:
                # when inside a row_header, check if is "Sum", which means we
                # ended the day data rows.
                # Setup the flags for other parts to know data should not be considered
                if "Sum" in data:
                    self.invalid_row = True
                    self.tbody = False
            # got inside the row header
            if self.day_data:
                # Get the date from the anchor tag
                if data.isnumeric():
                    # this means data has the day of the row stored
                    self.dict_day = data
                else:
                    self.day_data = False
            # self.get_data means we should grab the data from this tag.
            if self.get_data:
                # check for "\n" data and ignore
                # also ignore if invalid_row, which means we finished the day rows
                if "n" in data or self.invalid_row:
                    return
                # Now inside valid data columns
                if self.data_counter == 0:
                    # self.data_counter == 0 means first column for MaxTemp
                    # Data in the website will have a M inside when missing values
                    if "M" in data or data in "N":
                        self.dict_max_temp = "Missing"
                    elif (
                        data == "A"
                        or data == "C"
                        or data == "E"
                        or data == "F"
                        or data == "L"
                        or not data.strip()
                    ):
                        return
                    else:
                        self.dict_max_temp = float(data)
                    self.data_counter += 1
                elif self.data_counter == 1:
                    # self.data_counter == 1 means second column for MinTemp
                    # Data in the website will have a M inside when missing values
                    if "M" in data or data in "N":
                        self.dict_min_temp = "Missing"
                    elif (
                        data == "A"
                        or data == "C"
                        or data == "E"
                        or data == "F"
                        or data == "L"
                        or not data.strip()
                    ):
                        return
                    else:
                        self.dict_min_temp = float(data)
                    self.data_counter += 1
                elif self.data_counter == 2:
                    # self.data_counter == 2 means third column for MeanTemp
                    # Data in the website will have a M inside when missing values
                    if "M" in data or data in "N":
                        self.dict_mean_temp = "Missing"
                    elif (
                        data == "A"
                        or data == "C"
                        or data == "E"
                        or data == "F"
                        or data == "L"
                        or not data.strip()
                    ):
                        return
                    else:
                        self.dict_mean_temp = float(data)
                    self.data_counter += 1
                    self.get_data = False
                    # Now we have all data needed stored in variables
                    # Setup dictionary data:
                    date_key = f"{self.dict_year}-{self.dict_month}-{self.dict_day}"
                    dict_value = {
                        "Max": self.dict_max_temp,
                        "Min": self.dict_min_temp,
                        "Mean": self.dict_mean_temp,
                    }
                    self.weather_data[date_key] = dict_value
        except Exception as e:
            print("Error: ", e)

    def handle_endtag(self, tag):
        """Handles end tags to reset flags where needed"""
        try:
            # Leaving <tbody> tag
            if "tbody" in tag:
                self.tbody = False  # Left tbody tag
            # Leaving <tr> tag
            if self.tbody and "tr" in tag:
                self.entered_row = False
            # Leaving <td> tag. There are lots of unwanted <td> tags,
            # use data_counter to know we just went through the last useful column
            if "td" in tag and self.data_counter > 2:
                self.get_data = False
                self.entered_row = False  # Do not need to read any more row data
            # Leaving <th> tag, which is the row_header
            if self.tbody and self.entered_row and "th" in tag:
                self.row_header = False
            # Leaving <a> tag, so the Day data was already stored
            if self.tbody and self.row_header and "abbr" in tag:
                self.day_data = False
        except Exception as e:
            print("Error: ", e)


# Helper function for getting url with date data. Will be changed and
# reused later when we start implementing multiple requests to get each month data
def get_url(date_param):
    try:
        # today = datetime.today()
        end_year = date_param.strftime("%Y")
        search_year = date_param.strftime("%Y")
        search_month = date_param.strftime("%m")
        search_day = date_param.strftime("%d")
        url = "https://climate.weather.gc.ca/climate_data/daily_data_e.html?StationID=27174&timeframe=2&StartYear=1840&EndYear={}&Day={}&Year={}&Month={}".format(
            end_year, search_day, search_year, search_month
        )
        return url
    except Exception as e:
        print("Error: ", e)


if __name__ == "__main__":
    # Testing the class. Sends one request and checks the data parsed.
    today = datetime.today()
    year = today.strftime("%Y")
    month = today.strftime("%m")
    my_parser = ParseHTML(year, month)
    # Todays url
    # url = get_url(today)
    # Testing a specific url
    my_parser = ParseHTML("1996", "10")
    url = "https://climate.weather.gc.ca/climate_data/daily_data_e.html?StationID=27174&timeframe=2&StartYear=1840&EndYear=2020&Day=1&Year=1996&Month=10"
    # url = "https://climate.weather.gc.ca/climate_data/daily_data_e.html?StationID=27174&timeframe=2&StartYear=1840&EndYear=2020&Day=1&Year=1997&Month=07"

    # url = "https://climate.weather.gc.ca/climate_data/daily_data_e.html?StationID=27174&timeframe=2&StartYear=1840&EndYear=2020&Day=30&Year=2020&Month=11"
    print(url)
    with urllib.request.urlopen(url) as response:
        HTML = str(response.read())

    my_parser.feed(HTML)

    print("================================================")
    print("========== Data finished parsing ===============")
    print("================================================")
    for key, value in my_parser.weather_data.items():
        print(key, value)
    print("Found ", len(my_parser.weather_data), " Weather Data rows")

    print(f"Is there more data available? {my_parser.is_more_data_available}")
