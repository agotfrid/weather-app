"""
xxxxx
"""
import datetime


class Utility:
    """
    Utility class............
    """

    @staticmethod
    def get_url(year, month):
        """Returns the climate.weather.gc.ca with proper params, based on date_param
        date_param must be a datetime value, example:
        today = datetime.today()
        """
        try:
            today = datetime.datetime.today()
            end_year = today.strftime("%Y")
            search_year = year
            search_month = month
            url = (
                "https://climate.weather.gc.ca/climate_data/daily_data_e.html?StationID=27174"
                + "&timeframe=2&StartYear=1840&EndYear={}&Day=01&Year={}&Month={}"
            ).format(end_year, search_year, search_month)
            return url
        except Exception as e:
            print("Error: ", e)

    @staticmethod
    def get_month(url):
        """Return the month value extracted from the url string"""
        try:
            month = url.split("&Month=")[1][:2]
            if month[len(month) - 1] == "#":
                return f"0{month[:-1]}"
            else:
                return month
        except Exception as e:
            print("Error: ", e)

    @staticmethod
    def get_year(url):
        """Return the year value extracted from the url string"""
        try:
            return url.split("&Year=")[1][:4]
        except Exception as e:
            print("Error: ", e)

    @staticmethod
    def get_location(url):
        """Return the year value extracted from the url string"""
        try:
            return url.split("StationID=")[1][:5]
        except Exception as e:
            print("Error: ", e)

    @staticmethod
    def get_previous_month(month):
        """Return the previous month value"""
        try:
            date = datetime.datetime(2020, int(month), 1)
            previous_month = date - datetime.timedelta(days=1)
            return previous_month.strftime("%m")
        except Exception as e:
            print("Error: ", e)

    @staticmethod
    def get_previous_year(year):
        """Return the previous month value"""
        try:
            date = datetime.datetime(int(year), 1, 1)
            previous_year = date - datetime.timedelta(days=1)
            return previous_year.strftime("%Y")
        except Exception as e:
            print("Error: ", e)


if __name__ == "__main__":
    url = "https://climate.weather.gc.ca/climate_data/daily_data_e.html?StationID=27174&timeframe=2&StartYear=1840&EndYear=2020&Day=1&Year=1996&Month=10"
    print(Utility.get_location(url))