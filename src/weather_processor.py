# PART 4: Module with a WeatherProcessor class
"""
This Module acts as a weather processor
"""
from datetime import datetime
from utility.utility import Utility
from scrape_weather import WeatherScraper
from db_operations import DBOperations
from plot_operations import PlotOperations


class WeatherProcessor:
    """ Weather processor is here"""

    def __init__(self):
        """ Initialize class variables"""
        try:
            self.close = False
        except Exception as e:
            print("Error: ", e)

    ######################################
    def run_UI(self):
        """Run the loop showing the menu and getting the input,
        calling methods based on selection by the user"""
        try:
            while not self.close:
                self.print_menu()
                choice = self.read_input("Your option: ")
                ######################################
                if choice == "1":
                    # Fully download weather data
                    self.download_weather_data(False)
                ######################################
                elif choice == "2":
                    # Update weather data
                    self.download_weather_data(True)
                ######################################
                elif choice == "3":
                    start_y = self.read_input("Start year (yyyy): ")
                    end_y = self.read_input("End year (yyyy): ")
                    if (
                        len(start_y) == 4
                        and len(end_y) == 4
                        and start_y.isnumeric()
                        and end_y.isnumeric()
                    ):
                        # Plot weather data (year range)
                        self.plot_yearly_data(start_y, end_y)
                    else:
                        print("Invalid data entered, year must be in yyyy format.")
                ######################################
                elif choice == "4":
                    date = self.read_input("Select a year-month (yyyy-mm): ")
                    if (
                        len(date) == 7
                        and date[:4].isnumeric()
                        and date[5:7].isnumeric()
                    ):
                        # Plot weather data (year range)
                        self.plot_month_data(date)
                    else:
                        print("Invalid data entered, year must be in yyyy-mm format.")
                ######################################
                elif choice == "q":
                    print("Thank you for using Weather App...")
                    self.close = True
                ######################################
                else:
                    print("Input was invalid, please pick a value from the list!")

        except Exception as e:
            print("Error: ", e)

    ######################################
    def print_menu(self):
        """ Print out Weather app menu"""
        try:
            print()
            print("=" * 40)
            print("This is the Weather App, please choose an item from the menu below:")
            print("1. Fully download weather data.")
            print("2. Update weather data")
            print("3. Plot weather data (year range)")
            print("4. Plot weather data (month)")
            print("q. Quit Application")
        except Exception as e:
            print("Error: ", e)

    ######################################
    def read_input(self, message):
        """ Reads user choice input """
        try:
            user_input = input(message)
            return user_input
        except Exception as e:
            print("Error: ", e)

    ######################################
    def download_weather_data(self, is_update):
        """Downloads weather data using WeatherScraper class
        if is_update is set to True, will just update
        data based on what is currently in the database"""
        try:
            url = self.get_base_url()
            year = Utility.get_year(url)
            # Start the DB class
            dbname = "weather.sqlite"
            db_operation = DBOperations(dbname)
            if is_update:
                # Get the newest data in the database
                # Result returns yyyy-mm-dd
                result = db_operation.fetch_data(int(year), int(year), True)
                # stop_at_date will be one month backwards to make sure data was updated
                result_y = str(result)[:4]
                result_m = str(result)[5:7]
                result_m = Utility.get_previous_month(result_m)
                if result_m == "12":
                    result_y = Utility.get_previous_year(result_y)
                # stop_at_date should be yyyy-mm
                stop_at_date = f"{result_y}-{result_m}"

                # Scrappe all new data up until stop_at_date
                weather_scraper = WeatherScraper()
                weather_scraper.start_scraping(url, year, stop_at_date)
                # When done scrapping
                # Update DB with scrapped data
                db_operation.save_data(weather_scraper.weather)
                print(f"Weather data was updated successfully.")
            else:
                # Scrappe all data
                weather_scraper = WeatherScraper()
                weather_scraper.start_scraping(url, year)
                # When done scrapping
                # Reset DB data
                db_operation.purge_data()
                # Store scrapped DB data
                db_operation.save_data(weather_scraper.weather)
                print(f"Scrapped data from {len(weather_scraper.weather)} days.")
        except Exception as e:
            print("Error: ", e)

    ######################################
    def plot_yearly_data(self, start_year, end_year):
        """Plots yearly weather data using PlotOperations class
        :start_year: and :end_year: params must be in "yyyy" format"""
        try:
            PlotOperations("weather.sqlite").plot_yearly_data(
                int(start_year), int(end_year)
            )
        except Exception as e:
            print("Error: ", e)

    ######################################
    def plot_month_data(self, date):
        """Plots month weather data using PlotOperations class
        :date: param must be in "yyyy-mm" format"""
        try:
            year = date[:4]
            month = date[5:7]
            PlotOperations("weather.sqlite").plot_month_data(int(year), int(month))
        except Exception as e:
            print("Error: ", e)

    ######################################
    def get_base_url(self):
        """ Helper function for getting the base url with todays date data."""
        try:
            today = datetime.today()
            end_year = today.strftime("%Y")
            search_year = today.strftime("%Y")
            search_month = today.strftime("%m")
            search_day = today.strftime("%d")
            url = "https://climate.weather.gc.ca/climate_data/daily_data_e.html?StationID=27174&timeframe=2&StartYear=1840&EndYear={}&Day={}&Year={}&Month={}".format(
                end_year, search_day, search_year, search_month
            )
            return url
        except Exception as e:
            print("Error: ", e)


if __name__ == "__main__":
    interface = WeatherProcessor()
    interface.run_UI()
